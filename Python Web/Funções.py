
import enum
import shutil
import tempfile
from xml.etree.ElementTree import tostring



def html_style(html, linha, Style):
    newhtml = ""
        
    """
    html = Diretório da página do site;
    linha = Linha onde o Style deverá ser incrementado;
    Style = Diretório do arquivo.css que contém o Style daquela página ou do necessário.
    """
    
    linha = linha
    
    with open(Style,"r",encoding="utf-8") as Style:
        Style = Style.read()
        # print(Style)
     
    with open(html,"r",encoding="utf-8") as html:
        html = html.readlines()
        
        for i, line in enumerate(html):
            
            if i == linha:
                newhtml = newhtml + Style
            else:
                newhtml = newhtml + line
            
    return newhtml
    
    
        

# html = "Python Web\Paginas\Login.html"
# style = "Python Web\Styles\Style_Login.css"

# site = html_style(html,5,style)
# # print(site.name)