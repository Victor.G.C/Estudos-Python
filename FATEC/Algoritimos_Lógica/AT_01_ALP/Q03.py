import os


def Query(TIPO,CONSUMO):
    """_summary_

    Args:
        TIPO (String): Tipo de consumo do local
        CONSUMO (Int): Quantidade de consumo em kWh
    """
    
    tipo = TIPO
    consumo = CONSUMO
    
    if tipo == "Residencial":
        if consumo <= 500:
            preço = 0.40
        else:
            preço = 0.65
        
    elif tipo == "Comercial":
        if consumo <= 1000:
            preço = 0.55
        else:
            preço = 0.60

    elif tipo == "Industrial":
        if consumo <= 5000:
            preço = 0.55
        else:
            preço = 0.60
    
    return preço

def TipoResidencial():
    
    while True:
              
        print("="*100)
        print \
    ("""Bem vindo ao seviço de verificação de consumo EDP
    
    Insira o número abaixo correspondente ao tipo de consumo da residência:
    
    1 - Resindencial
    2 - Comercial
    3 - Industrial
    """)
        print("="*100)
        
        try:
            tipo = int(input('Qual o tipo de consumo da residência: '))
            
            if tipo == 1:
                tipo = "Residencial"
                break
            elif tipo == 2:
                tipo = "Comercial"
                break
            elif tipo == 3:
                tipo = "Industrial"
                break
            else:
                os.system('cls')
                print("="*100)
                print("Digite um número válido!!") 
                
        except:
            os.system('cls')
            print("="*100)
            print("Digite um número válido!!")

    return tipo


def Calcular():
    os.system('cls')
    
    tipo = TipoResidencial()
    print(tipo)
    
    while True:
        try:
            os.system('cls')
            print("="*100)
            print("Insira o consumo total do imóvel!\nEm kWh")
            print("\n" + "="*100)
            consumo = int(input('Consumo: '))
            break         
        
        except:
            
            os.system('cls')
            print("="*100)
            print("Digite um número válido!!") 
    
    custo = Query(tipo,consumo)
    print(custo)                  
        
    os.system('cls')
    
    Conta = consumo * custo
    Conta = "R$"+f'{round(Conta,2)}'
    
    
    Conta = Conta.replace('.',',')
    c = Conta.split(",")
    # print(c[1])
    
    if len(c[1]) == 1:
        Conta = Conta + "0"
        
    print("="*100)
    
    print\
(f"""O tipo de consumo é: {tipo}
O consumo total é: {consumo}kWh
O custo por kWh é: R${custo}
---------------------------------
A conta a ser paga é: {Conta}

OBRIGADO POR USAR NOSSOS SERVIÇOS!!
Para fazer uma nova consulta, digite 1
Para finalizar o atendimento, digite 0""")

    print("="*100)

    
        
while True:      
    Calcular()
    
    while True:
        try:
            nova_consulta = int(input('Nova consulta? '))
            
            
            if nova_consulta == 0:
                os.system('cls')
                break
            elif nova_consulta == 1:
                break
            else:
            
                os.system('cls')
                print("="*100)
                print("Digite apenas números válidos!!")
                print("1 para fazer nova consulta")
                print("0 para finalizar o atendimento")
        
        except:
            os.system('cls')
            print("="*100)
            print("Digite apenas números válidos!!")
            print("1 para fazer nova consulta")
            print("0 para finalizar o atendimento")
        
            
    if nova_consulta == 0:
        break